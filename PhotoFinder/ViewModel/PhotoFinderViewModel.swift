//
//  PhotoFinderViewModel.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 18/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import Foundation

protocol PhotoFinderViewModelProtocol {
    func outputReachedEnd()
    func coverUrl(for index: Int) -> String?
    func photosCount() -> Int
    var addPhotos: ((Int) -> ())! { get set }
}

protocol PhotoFinderCollectionViewModelProtocol: PhotoFinderViewModelProtocol {
    func inputTextChanged(with text: String)
    func outputReachedEnd()
    var updatePhotos: (() -> ())! { get set }
    var loadingPhotos: (() -> ())! { get set }
    var errorOccured: ((String) -> ())! { get set }
}

protocol PhotoFinderScrollViewModelProtocol: PhotoFinderViewModelProtocol {
    func originalUrl(for index: Int) -> String?
}

class PhotoFinderViewModel: PhotoFinderCollectionViewModelProtocol, PhotoFinderScrollViewModelProtocol {
    var apiManager: ApiManagerProtocol! {
        didSet {
            self.apiManager.photosFound = { [weak self] newPhotos in
                self.photos = newPhotos
                self.updatePhotos()
            }
            self.apiManager.photosAdded = { [weak self] newPhotos in
                if self.photos == nil {
                    self.photos = newPhotos
                    self.updatePhotos()
                } else {
                    self.photos! += newPhotos
                    self.addPhotos(newPhotos.count)
                }
            }
            self.apiManager.errorOccured = { [weak self] error in
                self.errorOccured(error)
            }
        }
    }
    var updatePhotos: (() -> ())!
    var addPhotos: ((Int) -> ())!
    var errorOccured: ((String) -> ())!
    var loadingPhotos: (() -> ())!
    var photos: [Photo]?
    
    func inputTextChanged(with text: String) {
        loadingPhotos()
        if apiManager == nil {
            apiManager = ApiManager()
        }
        apiManager.findPhotos(for: text)
    }
    
    func outputReachedEnd() {
        apiManager.loadAdditionalPhotos()
    }

    func photosCount() -> Int {
        return photos?.count ?? 0
    }
    
    func photo(for index: Int) -> Photo? {
        let range = 0..<photosCount()
        if photos != nil && range.contains(index) {
            return photos![index]
        }
        return nil
    }
    
    func coverUrl(for index: Int) -> String? {
        return photo(for: index)?.coverUrl
    }
    
    func originalUrl(for index: Int) -> String? {
        return photo(for: index)?.originalUrl
    }
}
