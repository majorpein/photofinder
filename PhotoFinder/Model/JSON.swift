//
//  JSON.swift
//  PhotoFinder
//
//  Created by Alexandro on 20/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import Foundation

protocol JSONValue {
    subscript(key: String) -> JSONValue? { get }
    subscript(index: Int) -> JSONValue? { get }
}

extension NSNull : JSONValue {
    subscript(key: String) -> JSONValue? { return nil }
    subscript(index: Int) -> JSONValue? { return nil }
}

extension NSNumber : JSONValue {
    subscript(key: String) -> JSONValue? { return nil }
    subscript(index: Int) -> JSONValue? { return nil }
}

extension NSString : JSONValue {
    subscript(key: String) -> JSONValue? { return nil }
    subscript(index: Int) -> JSONValue? { return nil }
}

extension NSArray : JSONValue {
    subscript(key: String) -> JSONValue? { return nil }
    subscript(index: Int) -> JSONValue? { return (0..<count).contains(index) ? JSON(self.object(at: index) as AnyObject) : nil }
}

extension NSDictionary : JSONValue {
    subscript(key: String) -> JSONValue? { return JSON(self[key] as AnyObject) }
    subscript(index: Int) -> JSONValue? { return nil }
}

func JSON(_ object: AnyObject?) -> JSONValue? {
    if let some : AnyObject = object {
        switch some {
        case let null as NSNull: return null
        case let number as NSNumber: return number
        case let string as NSString: return string
        case let array as NSArray: return array
        case let dict as NSDictionary: return dict
        default: return nil
        }
    } else {
        return nil
    }
}
