//
//  ApiManager.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 18/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import Foundation

let apiKey = "43kaO9ME61Vo3i7xYQUsNttrvVEXrVZd"
let apiHost = "http://api.giphy.com"
let apiPath = "/v1/gifs/search"

protocol ApiManagerProtocol {
    func findPhotos(for text: String)
    func loadAdditionalPhotos()
    
    var photosFound: (([Photo]) -> ())! { get set }
    var photosAdded: (([Photo]) -> ())! { get set }
    var errorOccured: ((String) -> ())! { get set }
}

struct Response: Codable {
    let pagination: Pagination
    let data: [ResponseData]
}

struct ResponseData: Codable {
    let images: [String:Image]
}

struct Image: Codable {
    let url: String?
}

struct Pagination: Codable {
    let offset: Int
    let count: Int
    let total_count: Int
}

class ApiManager: ApiManagerProtocol {
    var text = ""
    var offset = 0
    var didLoadAllPhotos = false
    var isLoading = false
    var photosFound: (([Photo]) -> ())!
    var photosAdded: (([Photo]) -> ())!
    var errorOccured: ((String) -> ())!
    
    func fakeLoad() {
        switch text {
        case "lul":
            photosFound([Photo(coverUrl: "lul", originalUrl: "luuul"), Photo(coverUrl: "lul", originalUrl: "luuul"), Photo(coverUrl: "lul", originalUrl: "luuul")])
        case "kek":
            photosFound([Photo(coverUrl: "kek", originalUrl: "keeek")])
        case "error":
            errorOccured("azaza lol")
        default:
            photosFound([Photo]())
        }
    }
    
    func findPhotos(for text: String) {
        self.text = text
        self.offset = 0
        self.didLoadAllPhotos = false
        self.isLoading = false
        requestPortionOfPhotos()
    }
    
    func loadAdditionalPhotos() {
        if !didLoadAllPhotos && !isLoading {
            requestPortionOfPhotos()
        }
    }
    
    func requestPortionOfPhotos() {
        cancelAllRequests()
        
        let urlString = apiHost + apiPath + "?api_key=" + apiKey + "&q=" + text + "&offset=" + String(offset)
        guard let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) else {
            DispatchQueue.main.async {
                self.isLoading = false
                self.errorOccured("Error".localized)
            }
            return
        }
        guard let url:URL = URL(string: urlStringEncoded) else {
            DispatchQueue.main.async {
                self.isLoading = false
                self.errorOccured("Error".localized)
            }
            return
        }
        let session = URLSession.shared
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard let responseData = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.errorOccured("Error".localized)
                }
                return
            }
            guard error == nil else {
                print(error!)
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.errorOccured(error!.localizedDescription)
                }
                return
            }
            /*
            if let dataString = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                print(dataString)
                
            }
            */
            let decoder = JSONDecoder()
            do {
                let response = try decoder.decode(Response.self, from: responseData)
                //print(response.data.first?.images["fixed_width_still"]?.url)original_still
                var photos = [Photo]()
                for responseData in response.data {
                    guard let coverUrl = responseData.images["fixed_width_still"]?.url, let originalUrl = responseData.images["original_still"]?.url else {
                        continue
                    }
                    let photo = Photo(coverUrl: coverUrl, originalUrl: originalUrl)
                    photos.append(photo)
                }
                if self.offset == 0 {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.photosFound(photos)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.photosAdded(photos)
                    }
                }
                self.offset += response.pagination.count
                self.didLoadAllPhotos = response.pagination.offset == response.pagination.total_count
            } catch {
                print("error trying to convert data to JSON")
                print(error)
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.errorOccured("Error".localized)
                }
            }
        }
        
        isLoading = true
        task.resume()
    }
    
    func cancelAllRequests() {
        URLSession.shared.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }
}
