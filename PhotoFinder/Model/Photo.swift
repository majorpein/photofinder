//
//  Photo.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 19/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import Foundation

struct Photo {
    let coverUrl: String
    let originalUrl: String
    init(coverUrl: String, originalUrl: String) {
        self.coverUrl = coverUrl
        self.originalUrl = originalUrl
    }
}
