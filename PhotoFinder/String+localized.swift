//
//  String+localized.swift
//  PhotoFinder
//
//  Created by Alexandro on 20/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
