//
//  PhotoFinderCollectionViewCell.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 19/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import UIKit

class PhotoFinderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var coverView: UIImageView!
    
}
