//
//  FullPhotoView.swift
//  PhotoFinder
//
//  Created by Alexandro on 19/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import UIKit

class FullPhotoView: UIImageView {

    var isFullResShown = false
    var isLoading = false
    var activityIndicator: UIActivityIndicatorView?
}
