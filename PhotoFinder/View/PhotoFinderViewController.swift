//
//  PhotoFinderViewController.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 18/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import UIKit

let itemsCount = 4

enum State {
    case isNormal, isEmpty, isError, isLoading, isStart
}

class PhotoFinderViewController: UIViewController, UISearchBarDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    
    var photoViewModel: PhotoFinderCollectionViewModelProtocol! {
        didSet {
            self.photoViewModel.updatePhotos = { [weak self] in
                guard self != nil else {
                    return
                }
                self!.collectionView.reloadData()
                self!.show(state: self!.photoViewModel.photosCount() > 0 ? .isNormal : .isEmpty)
            }
            self.photoViewModel.addPhotos = { [weak self] photosCount in
                guard self != nil else {
                    return
                }
                let range = (self!.photoViewModel.photosCount() - photosCount)..<self!.photoViewModel.photosCount()
                var indexPaths = [IndexPath]()
                for index in range {
                    indexPaths.append(IndexPath(item: index, section: 0))
                }
                self!.collectionView.insertItems(at: indexPaths)
                self!.show(state: self!.photoViewModel.photosCount() > 0 ? .isNormal : .isEmpty)
                if let controller = self!.fullPhotoViewController {
                    controller.updateContentSize()
                }
            }
            self.photoViewModel.errorOccured = { [weak self] error in
                self?.errorLabel.text = error
                self?.errorLabel.isHidden = false
                self?.show(state: .isError)
            }
            self.photoViewModel.loadingPhotos = { [weak self] in
                self?.show(state: .isLoading)
            }
        }
    }
    
    var fullPhotoViewController: FullPhotoViewController! {
        didSet {
            self.fullPhotoViewController.closeClicked = { [weak self] in
                guard self != nil else {
                    return
                }
                let indexPath = IndexPath(item: self!.fullPhotoViewController.currentIndex, section: 0)
                let imageFrame = self!.getScreenCoordinatesOfItem(at: indexPath)
                self!.fullPhotoViewController.removeFromSuperview(to: imageFrame, animated: true)
            }
            self.fullPhotoViewController.scrolledToItem = { [weak self] index in
                self?.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredVertically, animated: true)
            }
            self.fullPhotoViewController.photosAdded = { [weak self] photosCount in
                guard self != nil else {
                    return
                }
                let range = (self!.photoViewModel.photosCount() - photosCount)..<self!.photoViewModel.photosCount()
                var indexPaths = [IndexPath]()
                for index in range {
                    indexPaths.append(IndexPath(item: index, section: 0))
                }
                self!.collectionView.insertItems(at: indexPaths)
                self!.show(state: self!.photoViewModel.photosCount() > 0 ? .isNormal : .isEmpty)
            }
        }
    }
    
    func show(state: State) {
        errorLabel.isHidden = state != .isError
        emptyLabel.isHidden = state != .isEmpty
        startLabel.isHidden = state != .isStart
        collectionView.isHidden = state != .isNormal
        if state == .isLoading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        photoViewModel = PhotoFinderViewModel()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        show(state: .isStart)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var tap: UITapGestureRecognizer!
    
    func hideKeyboardWhenTappedAround() {
        if tap == nil {
            tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        }
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        view.removeGestureRecognizer(tap)
    }
    
    @objc func handleRefresh() {
        collectionView.refreshControl?.endRefreshing()
        guard let text = searchBar.text else {
            return
        }
        if text.isEmpty {
            show(state: .isStart)
        } else {
            photoViewModel.inputTextChanged(with: text)
        }
    }
    
    // MARK: - Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        handleRefresh()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        hideKeyboardWhenTappedAround()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    // MARK: - CollectionView Delegate Flow Layout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width
        let itemWidth = width == 0.0 ? 0.0 : (width - 5.0 * CGFloat(itemsCount + 1)) / CGFloat(itemsCount)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    // MARK: - CollectionView Datasource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoViewModel.photosCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoFinderCollectionViewCellIdentifier", for: indexPath) as! PhotoFinderCollectionViewCell
        cell.coverView.image = nil
        if let coverUrl = photoViewModel.coverUrl(for: indexPath.item) {
            cell.coverView.loadImageUsingCache(withUrl: coverUrl)
        }
        return cell
    }
    
    // MARK: - CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Item \(indexPath.item) selected")
        fullPhotoViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FullPhotoViewController") as? FullPhotoViewController
        fullPhotoViewController.start(frame: getScreenCoordinatesOfItem(at: indexPath), index: indexPath.item)
        fullPhotoViewController.photoManager = photoViewModel as! PhotoFinderScrollViewModelProtocol
        view.addSubview(fullPhotoViewController.view)
    }
    
    func getScreenCoordinatesOfItem(at indexPath: IndexPath) -> CGRect {
        guard let cell = collectionView.cellForItem(at: indexPath) else {
            return CGRect.zero
        }
        let cellOrigin = cell.frame.origin
        let viewOrigin = collectionView.bounds.origin
        let cellSize = cell.frame.size
        return CGRect(x: cellOrigin.x, y: cellOrigin.y - viewOrigin.y + collectionView.frame.origin.y - view.safeAreaInsets.top, width: cellSize.width, height: cellSize.height)
    }
    
    // MARK: - ScrollView delegate

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let indexPaths = collectionView.indexPathsForVisibleItems.sorted{ $0.item < $1.item }
        guard let indexPath = indexPaths.last else {
            return
        }
        
        if photoViewModel.photosCount() - indexPath.item < 10 {
            photoViewModel.outputReachedEnd()
        }
    }
}
