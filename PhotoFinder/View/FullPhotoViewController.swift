//
//  FullPhotoViewController.swift
//  PhotoFinder
//
//  Created by Alexander Anosov on 18/10/2017.
//  Copyright © 2017 Anosov. All rights reserved.
//

import UIKit

class FullPhotoViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var closeButton: UIButton!
    
    var startIndex: Int!
    var currentIndex: Int! {
        didSet {
            self.scrolledToItem(self.currentIndex)
        }
    }
    var startFrame: CGRect!
    var closeClicked: (() -> ())!
    var photosAdded: ((Int) -> ())!
    var scrolledToItem: ((Int) -> ())!
    
    var photoViewModel: PhotoFinderScrollViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentIndex = startIndex
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadContent()
        animatedStart()
        
        scroll(to: currentIndex, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCloseClicked(_ sender: Any) {
        closeClicked()
    }
    
    func photo(with tag: Int) -> FullPhotoView? {
        return scrollView.viewWithTag(-tag-1) as? FullPhotoView
    }
    
    func createImage(for index: Int) -> FullPhotoView {
        if let imageView = photo(with: index) {
            return imageView
        }
        let imageView = FullPhotoView(frame: scrollView.bounds.offsetBy(dx: CGFloat(index) * scrollView.frame.width, dy: 0.0))
        imageView.contentMode = .scaleAspectFit
        imageView.tag = -index-1
        scrollView.addSubview(imageView)
        return imageView
    }
    
    func loadImage(for index: Int) {
        let range = 0..<photoViewModel.photosCount()
        guard range.contains(index) else {
            return
        }
        let imageView = createImage(for: index)
        imageView.frame = scrollView.frame.offsetBy(dx: CGFloat(index) * scrollView.frame.width, dy: 0.0)
        if !imageView.isLoading && !imageView.isFullResShown {
            imageView.isLoading = true
            
            imageView.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            imageView.addSubview(imageView.activityIndicator!)
            imageView.activityIndicator?.translatesAutoresizingMaskIntoConstraints = false
            imageView.activityIndicator?.hidesWhenStopped = true
            
            view.addConstraint(NSLayoutConstraint(item: imageView.activityIndicator!, attribute: .centerX, relatedBy: .equal, toItem: imageView, attribute: .centerX, multiplier: 1.0, constant: 0.0))
            view.addConstraint(NSLayoutConstraint(item: imageView.activityIndicator!, attribute: .centerY, relatedBy: .equal, toItem: imageView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
            
            imageView.activityIndicator?.startAnimating()
            
            if let originalUrl = photoViewModel.originalUrl(for: index) {
                DispatchQueue.global(qos: .userInitiated).async {
                    let data = try? Data(contentsOf: URL(string: originalUrl)!)
                    if let unwrappedData = data {
                        let image = UIImage(data: unwrappedData)
                        DispatchQueue.main.async {
                            imageView.image = image
                            imageView.activityIndicator!.stopAnimating()
                            imageView.isLoading = false
                            imageView.isFullResShown = true
                        }
                    }
                }
            }
        }
    }
    
    func removeImage(for index: Int) {
        let range = 0..<photoViewModel.photosCount()
        guard range.contains(index) else {
            return
        }
        if let imageView = photo(with: index) {
            if imageView.isFullResShown {
                imageView.image = nil
                imageView.activityIndicator!.stopAnimating()
                imageView.isFullResShown = false
            }
        }
    }
    
    // MARK: - ScrollView Delegate
    
    func scroll(to index: Int, animated: Bool) {
        let frame = scrollView.frame.offsetBy(dx: CGFloat(index) * scrollView.frame.width, dy: 0.0)
        scrollView.scrollRectToVisible(frame, animated: animated)
        currentIndex = index
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func setCurrentIndexHard() {
        currentIndex = Int((scrollView.contentOffset.x + scrollView.frame.width/2.0) / scrollView.frame.width)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let prevIndex = currentIndex!
        setCurrentIndexHard()
        var step = 1
        if currentIndex < prevIndex {
            step = -1
        }
        
        loadImage(for: currentIndex + step)
        loadImage(for: currentIndex)
        loadImage(for: currentIndex - step)
        removeImage(for: currentIndex - step * 2)
        
        if photoViewModel.photosCount() - currentIndex < 5 {
            photoViewModel.outputReachedEnd()
        }
    }
    
    func start(frame: CGRect, index: Int) {
        startFrame = frame
        startIndex = index
    }
    
    func setBackgroundVisible(_ visible: Bool) {
        view.backgroundColor = UIColor(white: 0.0, alpha: visible ? 1.0 : 0.0)
        scrollView.backgroundColor = UIColor(white: 0.0, alpha: visible ? 1.0 : 0.0)
        closeButton.isHidden = !visible
    }
    
    func animatedStart() {
        guard let imageView = photo(with: startIndex) else {
            return
        }
        imageView.contentMode = .scaleAspectFit
        imageView.frame = startFrame.offsetBy(dx: CGFloat(self.currentIndex) * self.scrollView.frame.width, dy: 0.0)
        if let coverUrl = photoViewModel.coverUrl(for: startIndex) {
            imageView.loadImageUsingCache(withUrl: coverUrl)
        }
        imageView.isLoading = true
        
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        scrollView.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        UIView.animate(withDuration: 0.3, animations: {
            imageView.frame = self.scrollView.bounds.offsetBy(dx: CGFloat(self.currentIndex) * self.scrollView.frame.width, dy: 0.0)
        }, completion: { finished in
            imageView.isLoading = false
            self.setBackgroundVisible(true)
            self.loadImage(for: self.currentIndex)
        })
    }
    
    func updateContentSize() {
        scrollView.contentSize = CGSize(width: CGFloat(photoViewModel.photosCount()) * scrollView.frame.width, height: scrollView.frame.height)
    }
    
    func reloadContent() {
        
        scrollView.subviews.forEach { $0.removeFromSuperview() }
        
        updateContentSize()
        
        _ = createImage(for: currentIndex)
        loadImage(for: currentIndex + 1)
        loadImage(for: currentIndex - 1)
    }
    
    func removeFromSuperview(to frame: CGRect, animated: Bool) {
        func completion() {
            self.setBackgroundVisible(true)
            self.view.removeFromSuperview()
            
            self.removeImage(for: self.currentIndex);
            self.removeImage(for: self.currentIndex+1);
            self.removeImage(for: self.currentIndex-1);
        }
        
        guard let imageView = photo(with: currentIndex) else {
            completion()
            return
        }
        
        self.setBackgroundVisible(false)
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                imageView.frame = frame.offsetBy(dx: CGFloat(self.currentIndex) * self.scrollView.frame.width, dy: 0.0)
            }, completion: { finished in
                completion()
            })
        } else {
            completion()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
